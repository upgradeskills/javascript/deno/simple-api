import { Application, Context } from "https://deno.land/x/abc@v1.3.2/mod.ts";
//import { stdOutStream } from "https://deno.land/x/stdoutstream@1.0/mod.ts";
import { getAllSahams , getSaham, addSaham, updateSaham, deleteSaham} from './controllers/sahamController.ts';
import { login , register} from  './controllers/authController.ts';
// import { authorize } from './middleware/authorize.ts';
const app = new Application();

app.static('/', './public');
app.get('/', async(_ctx: Context) => {
    await _ctx.json({
        'message' :'welcome to index saham create by Rhamad Nursani Sidik'
    });
});

app.post('/index', async(_ctx: Context) => {

const process = Deno.run({
 cmd: ["ping","-c", "1", "google.com"],
 stdout: "piped",
 stderr: "piped"
});

//Deno.exit(0);
const output = await process.output(); // "piped" must be set
const outStr = new TextDecoder().decode(output);
const regex = /time=(.*?) ms/i;
//const a  = 'ok';
//process.close();

//const process = new stdOutStream();
//process.run("ping", "google.com");
console.log(outStr);
   await _ctx.json({
        'ms': Math.floor(Math.random() * 6) + 1,
        'status': 1,
        'message': 'invalid ssl',
        'ip_uptime_exec': '103.160.37.141',
        'ping_result' : outStr.match(regex)
   });

});



//auth
app.post('/login', login)
    .post('/register', register);

//user

//saham
app.get('/saham', getAllSahams)
    .post('/saham', addSaham)
    .get('/saham/:id', getSaham)
    .put('/saham/:id', updateSaham)
    .delete('/saham/:id', deleteSaham);

//
app.start({port:3000});
